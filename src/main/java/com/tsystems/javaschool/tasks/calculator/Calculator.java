package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            RPN rpn = new RPN(statement);
            double res = rpn.evaluate();

            return new DecimalFormat("#.####").format(res);
        } catch (Exception e) {
            //System.out.println(e.getMessage());
            return null;
        }
    }

}
