package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class RPN {

    private interface Operator {
        int getPriority();
        default double evaluate(double a, double b) throws Exception {
            throw new Exception("Not supported");
        }
    }

    private ArrayList<String> rpn = new ArrayList<>();

    private static final Map<String, Operator> symbols = createOperatorsMap();

    private static Map<String, Operator> createOperatorsMap() {
        Map<String,Operator> symbols = new HashMap<>();
        symbols.put("*", new Operator() {
            public int getPriority() { return 3; }
            public double evaluate(double a, double b) { return a * b; }
        });

        symbols.put("/", new Operator() {
            public int getPriority() { return 3; }
            public double evaluate(double a, double b) throws Exception {
                if (b == 0.0) throw new Exception("Divide by 0");
                return a / b;
            }
        });
        symbols.put("+", new Operator() {
            public int getPriority() { return 2; }
            public double evaluate(double a, double b) { return a + b; }
        });

        symbols.put("-", new Operator() {
            public int getPriority() { return 2; }
            public double evaluate(double a, double b) { return a - b; }
        });
        symbols.put("(", () -> 1);
        symbols.put(")", () -> 1);
        return symbols;
    }

    RPN(String expression) throws Exception {
        verify(expression);
        parse(expression);
    }

    /**
     * Evaluate rpn expression
     *
     * @return result of expression
     */
    double evaluate() throws Exception {
        Stack<Double> numbers = new Stack<>();

        for (String elem : rpn) {
            if (isNumeric(elem)) numbers.push(Double.parseDouble(elem));
            else compute(elem, numbers);
        }
        double res = numbers.pop();

        if (!numbers.empty()) throw new Exception("Extra number '" + numbers.pop() + "' was found");

        return res;
    }

    /**
     * Evaluate one operator in rpn stack. Push result at the top of the stack.
     *
     */
    private void compute(String operation, Stack<Double> numbers) throws Exception {
        if (numbers.empty()) throw new Exception("Extra operation '" + operation + "' was found");
        double a = numbers.pop();
        if (numbers.empty()) throw new Exception("Extra operation '" + operation + "' was found");
        double b = numbers.pop();

        double res = symbols.get(operation).evaluate(b, a);
        numbers.push(res);
    }

    /**
     * looking for some of the mistakes
     *
     * @param expression expression in Infix notation
     *
     */
    private void verify(String expression) throws Exception {
        expression = expression.replaceAll(" ", "");

        if (expression.matches("\\(\\)")) throw new Exception("Forbidden sequence '()' was found");
    }

    /**
     * Converting infix to RPN, result will be in  'private ArrayList<String> rpn'
     *
     * @param expression expression in infix form
     *
     */
    private void parse(String expression) throws Exception {
        String[] splitExpression = split(expression);
        Stack<String> stack = new Stack<>();

        for (String elem : splitExpression) {

            if (isNumeric(elem)) rpn.add(elem);
            else if (isSymbol(elem)) {
                if (elem.equals("(")) stack.push(elem);
                else if (elem.equals(")")) {
                    removeParentheses(stack, rpn);
                } else {
                    updateStack(stack, rpn, symbols.get(elem).getPriority());
                    stack.push(elem);
                }
            } else throw new Exception("Can't parse symbol '" + elem + "'");
        }
        releaseStack(stack, rpn);
    }

    /**
     * split infix expression,
     *
     * @param expression expression in infix form
     *
     * @return splitted expression
     */
    private String[] split(String expression) {
        expression = expression.replaceAll(" ", "");

        StringBuilder symbolsRegex = new StringBuilder();
        for (String symbol : symbols.keySet()) {
            if (symbol.equals("-")) symbolsRegex.append("\\-");
            else symbolsRegex.append(symbol);
        }
        String expressionRegex = String.format("((?<=[%1$s])|(?=[%1$s]))", symbolsRegex.toString());

        return expression.split(expressionRegex);
    }

    private static boolean isNumeric(String val) {
        if (val == null) return false;
        return val.matches("-?\\d+(\\.\\d+)?");
    }

    private static boolean isSymbol(String val) {
        if (val == null) return false;
        return symbols.containsKey(val);
    }

    /**
     * moves elements from @stack to @rpn till element priority will be less than @priority
     *
     */
    private void updateStack(Stack<String> stack, ArrayList<String> rpn, int priority) {
        while (!stack.empty() && symbols.get(stack.peek()).getPriority() >= priority) {
            rpn.add(stack.pop());
        }
    }

    /**
     * moves all elements from @stack to @rpn
     *
     */
    private void releaseStack(Stack<String> stack, ArrayList<String> rpn) throws Exception {
        while (!stack.empty()) {
            String elem = stack.pop();
            if (elem.equals("(")) throw new Exception("Extra character '(' was found");
            rpn.add(elem);
        }
    }

    /**
     * moves elements from @stack to @rpn till '(' is found
     *
     */
    private void removeParentheses(Stack<String> stack, ArrayList<String> rpn) throws Exception {
        while (!stack.empty()) {
            String elem = stack.pop();
            if (elem.equals("(")) return;
            rpn.add(elem);
        }
        throw new Exception("Can't find symbol '('");
    }
}