package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null) throw new IllegalArgumentException();

        if (!canBuildPyramid(inputNumbers)) throw new CannotBuildPyramidException();

        int depth = getPyramidDepth(inputNumbers.size());
        int[][] res = new int[depth][2 * depth - 1];

        Collections.sort(inputNumbers);
        fillPyramid(inputNumbers, res);

        return res;
    }

    private boolean canBuildPyramid(List<Integer> inputNumbers) {
        int size = inputNumbers.size();
        if (size < 1 || inputNumbers.contains(null)) return false;
        return Math.sqrt(1 + 8*size) % 1 == 0;  // <= (size = sum(i = 1:n)[i])
    }

    private int getPyramidDepth(int size) {
        return (int)(-1 + Math.sqrt(1 + 8*size)) / 2;  // <=> (size = sum(i = 1:n)[i])  #n - depth
    }

    private void fillPyramid(List<Integer> inputNumbers, int[][] res) {
        int id = 0;
        for (int i = 0; i < res.length; ++i) {
            int pos = res[0].length / 2 - i;
            for (int j = 0; j <= i; ++j)
                res[i][pos + j*2] = inputNumbers.get(id++);
        }
    }
}
